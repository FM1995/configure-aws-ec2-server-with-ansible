# Configure AWS EC2 server with Ansible

#### Project Outline

We will use our Ansible machine so that it is able to connect to AWS instances

#### Lets get started

Here we have a t2.micro instance along with a created private key ‘Ansible-New-Linux.pem’

![Image1](https://gitlab.com/FM1995/configure-aws-ec2-server-with-ansible/-/raw/main/Images/Image1.png)

Can then configure the below hosts along with its variables separating out the digital ocean servers and the AWS servers

![Image2](https://gitlab.com/FM1995/configure-aws-ec2-server-with-ansible/-/raw/main/Images/Image2.png)

Can then run the below for the ansible control machine to check the connection the ec2 server

```
ansible ec2 -I hosts -m ping
```

![Image3](https://gitlab.com/FM1995/configure-aws-ec2-server-with-ansible/-/raw/main/Images/Image3.png)
